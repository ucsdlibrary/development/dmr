# encoding: utf-8
def set_current_user(user=nil)
  user = User.find_or_create_for_developer(nil)
  session[:user_id] = user.uid
end

# sign in as a developer
def sign_in_developer
  visit new_user_session_path
end

# sign out as a developer
def sign_out_developer
  visit destroy_user_session_path
end

def omniauth_setup_developer
  OmniAuth.config.test_mode = true
  OmniAuth.config.mock_auth[:shibboleth] = OmniAuth::AuthHash.new({
    provider: 'developer',
    uid: '1',
    info: { 'email' => 'developer@ucsd.edu', 'name' => 'developer' }
  })
  Rails.application.env_config['omniauth.auth'] = OmniAuth.config.mock_auth[:shibboleth]
end

def omniauth_setup_shibboleth
  OmniAuth.config.test_mode = true
  OmniAuth.config.mock_auth[:shibboleth] = OmniAuth::AuthHash.new({
    provider: 'shibboleth',
    uid: 'omniauth_test',
    info: { 'email' => 'test_user@ucsd.edu', 'name' => 'test_user' }
  })
  Rails.application.env_config['omniauth.auth'] = OmniAuth.config.mock_auth[:shibboleth]
end

def mock_administrator
  allow(User).to receive(:in_group?).and_return(true)
end

def mock_non_administrator
  allow(User).to receive(:in_group?).and_return(false)
end

def mock_student
  allow(User).to receive(:student?).and_return(true)
end

def mock_non_student
  allow(User).to receive(:student?).and_return(false)
end
