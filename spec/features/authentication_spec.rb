# encoding: utf-8
require 'spec_helper'

feature 'Authentication' do
  before do
    Rails.configuration.shibboleth = true
  end

  after do
    Rails.configuration.shibboleth = false
  end

  context 'as a developer' do
    before do
      mock_administrator
      mock_non_student
      omniauth_setup_developer
    end
    
    it 'enforces authentication' do
      visit root_path
      expect(page).to have_content('Sign Out')
    end

    it 'redirects to logout url on sign out' do
      sign_in_developer
      sign_out_developer
      expect(page).to have_current_path(logout_path)
      expect(page).to have_content('DMR Logged out')
      expect(page).to have_content('Sign In')
    end
  end

  context 'as a administrator with shibboleth' do
    before do
      mock_administrator
      mock_non_student
      omniauth_setup_shibboleth
    end

    it 'allows access for administrator' do
      sign_in_developer
      expect(page).to have_content('Sign Out')
    end

    it 'redirects to logout url on sign out' do
      sign_in_developer
      sign_out_developer
      expect(page).to have_current_path(logout_path)
      expect(page).to have_content('DMR Logged out')
      expect(page).to have_content('Sign In')
    end
  end
  
  context 'as a student with shibboleth' do
    before do
      mock_non_administrator
      mock_student
      omniauth_setup_shibboleth
    end

    it 'does not allow access for student' do
      sign_in_developer
      expect(page).to have_content('Forbidden')
    end
  end
end
