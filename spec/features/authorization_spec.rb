# encoding: utf-8
require 'spec_helper'

feature 'Authorization' do
  before(:all) do
    Rails.configuration.shibboleth = true
    @course = Course.create course: 'Test Course 1', instructor: 'Test Instructor 1', year: '2021', quarter: 'Spring'
  end

  after(:all) do
    Rails.configuration.shibboleth = false
    @course.delete
  end

  context 'as a developer' do
    before do
      mock_administrator
      mock_non_student
      omniauth_setup_developer
      sign_in_developer
    end

    it 'allows access to course playlist' do
      sign_in_developer
      visit course_path(@course)
      expect(page).to have_content('Test Course 1 Spring 2021')
    end
    
    it 'allows access to edit course page' do
      visit edit_course_path(@course)
      expect(page).to have_selector("input#course_course[value='Test Course 1']")
    end
  end

  context 'as a administrator with shibboleth' do
    before do
      mock_administrator
      mock_non_student
      omniauth_setup_shibboleth
      sign_in_developer
    end

    it 'allows access to course playlist' do
      visit course_path(@course)
      expect(page).to have_content('Test Course 1 Spring 2021')
    end
    
    it 'allows access to edit course page' do
      visit edit_course_path(@course)
      expect(page).to have_selector("input#course_course[value='Test Course 1']")
    end
  end

  context 'as a student with shibboleth' do
    before do
      mock_non_administrator
      mock_student
      omniauth_setup_shibboleth
      sign_in_developer
    end

    it 'allows access to course playlist' do      
      visit course_path(@course)
      expect(page).to have_content('Test Course 1 Spring 2021')
    end
    
    it 'does not allow access to edit course page' do
      visit edit_course_path(@course)
      expect(page).to have_content('Forbidden')
    end
  end
end
