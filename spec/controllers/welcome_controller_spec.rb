# encoding: utf-8
#---
# @author Vivian <tchu@ucsd.edu>
#---

require 'spec_helper'

describe WelcomeController do
  describe 'GET index' do
    it 'renders the index page before Sign In' do
      set_current_user
      get :index 
      expect(response).to render_template :index
    end
  end
end
