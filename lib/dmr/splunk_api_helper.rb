#
# @author Vivian <tchu@ucsd.edu>
#

require 'splunk-sdk-ruby'
require 'date'
require 'time_diff'

module Dmr
  #
  # Collection of methods to support the Analytics Controller
  #
  module SplunkApiHelper
    WOWZAHOST = Rails.configuration.wowza_baseurl.split('/')[0]
    S_HOST = Rails.configuration.splunk_host
    RAILS_HOST = Rails.configuration.rails_host
    TOTAL = '| stats count as Total'
    AUDIO_MIN_PLAYTIME = 22_000

    def stats(query, start_date, end_date)
      service = Splunk.connect(host: Rails.application.secrets.splunk_uri,
                           username: Rails.application.secrets.splunk_username,
                           password: Rails.application.secrets.splunk_password)
      Rails.logger.info("stats query #{query} - start_date #{start_date} end_date #{end_date}")
      job = service.create_oneshot(query, earliest_time: start_date, latest_time: end_date,
                                   count: 100_000, offset: 0)
      Splunk::ResultsReader.new(job)
    end

    def data_count(query, s_date, e_date)
      count = 0
      data = stats(query, s_date, e_date)
      data.each do |result|
        count = result['Total']
      end
      count.to_i
    end

    def licensed_data(query, s_date, e_date)
      count = 0
      data = stats(query, s_date, e_date)

      data.each do |result|
        tmp = result['_raw'].delete('"').delete(' ')
        tmp_result = tmp.split(/},commit=>Save/)[0].split(/end_date=>/)[1]
        count += 1 if tmp_result.present?
      end
      count.to_i
    end

    def stop_time_licensed_view(files, s_date, e_date)
      filenames = {}
      files.each do |name|
        q = "search sourcetype=wowza_access host=#{WOWZAHOST} stop stream 200 dmr #{name}"
        data = stats(q, s_date, e_date)
        data.each do |result|
          tmp_result = result['_raw'].to_s.split
          filenames[process_id(tmp_result)] = tmp_result[1]
        end
      end
      filenames
    end

    def process_filename(query, s_date, e_date)
      data = stats(query, s_date, e_date)
      filename_array = []
      data.each do |result|
        tmp = result['_raw'].delete('"').delete(' ')
        if tmp.include?('file_name=>')
          tmp_file_name = tmp.split(/file_name=>/)[1].split(/}/)[0]
          filename_array << tmp_file_name if !filename_array.include?(tmp_file_name)
        end
      end

      filename_array
    end

    def new_item_count(s_date, e_date)
      q = "search sourcetype=rails host=#{RAILS_HOST} Add to Course List NOT audio"
      data = stats(q, s_date, e_date)
      id_count = 0
      data.each do |result|
        tmp = result['_raw'].delete('"').delete(' ')
        tmp_result = tmp.split(/media_ids=>\[/)[1]
        id_count += tmp_result.split(/\]/).first.split(',').length if !tmp_result.nil?
      end
      id_count
    end

    def new_audio_count(s_date, e_date)
      q = "search sourcetype=rails host=#{RAILS_HOST} Add to Course List audio"
      data = stats(q, s_date, e_date)
      id_count = 0
      data.each do |result|
        tmp = result['_raw'].delete('"').delete(' ')
        tmp_result = tmp.split(/media_ids=>\[/)[1]
        # Rails.logger.info("in new audio_count #{tmp_result}")
        id_count += tmp_result.split(/\]/).first.split(',').length if !tmp_result.nil?
      end
      id_count
    end

    def new_course_count(s_date, e_date)
      q = "search sourcetype=access_common host=#{S_HOST} uri=\"/dmr/courses/new\" 200 #{TOTAL}"
      data_count(q, s_date, e_date)
    end

    def audio_course_count(s_date, e_date)
      q = "search sourcetype=rails host=#{RAILS_HOST} Add to Course List audio"
      data = stats(q, s_date, e_date)
      course_count(data, 'audio')
    end

    def clone_audio_course_count(s_date, e_date)
      q = "search sourcetype=access_common host=#{S_HOST} dmr/courses/clone 302"
      data = stats(q, s_date, e_date)
      clone_course_count(data, 'audio')
    end

    def video_course_count(s_date, e_date)
      q = "search sourcetype=rails host=#{RAILS_HOST} Add to Course List NOT audio"
      data = stats(q, s_date, e_date)
      course_count(data, 'video')
    end

    def clone_video_course_count(s_date, e_date)
      q = "search sourcetype=access_common host=#{S_HOST} dmr/courses/clone 302"
      data = stats(q, s_date, e_date)
      clone_course_count(data, 'video')
    end

    # rubocop:disable Metrics/AbcSize
    def course_count(data, type)
      course_array = []
      data.each do |result|
        tmp_result = result['_raw'].delete('"').delete(' ').split(/media_ids=>\[/)[1]
        id = tmp_result.split(/\]/).first.split(',').first if !tmp_result.nil?
        report = Audioreport.where(audio_id: id.to_i).order('created_at DESC') if type == 'audio'
        report = Report.where(media_id: id.to_i).order('created_at DESC') if type == 'video'
        tmp_course_id = report.first.course_id if !report.nil? && !report.first.nil?
        course_array << tmp_course_id if !course_array.include?(tmp_course_id)
      end
      course_array.length
    end
    # rubocop:enable Metrics/AbcSize

    def clone_course_count(data, type)
      course_array = []
      report = nil
      data.each do |result|
        tmp_result = result['_raw'].delete('"').delete(' ')
        id = tmp_result.split('id=')[1].split('HTTP/').first
        if type == 'audio'
          report = Audioreport.where(course_id: id.to_i)
        else
          report = Report.where(course_id: id.to_i)
        end
        course_array << id if !report.nil? && !course_array.include?(id)
      end
      course_array.length
    end

    def new_record_count(s_date, e_date)
      q = "search sourcetype=access_common host=#{S_HOST} uri=/dmr/media/new 200 #{TOTAL}"
      data_count(q, s_date, e_date)
    end

    def licensed_audio_count(s_date, e_date)
      q = "search sourcetype=rails host=#{RAILS_HOST} AudiosController#create"
      licensed_data(q, s_date, e_date)
    end

    def licensed_video_count(s_date, e_date)
      q = "search sourcetype=rails host=#{RAILS_HOST} MediaController#create"
      licensed_data(q, s_date, e_date)
    end

    def new_audio_record_count(s_date, e_date)
      q = "search sourcetype=access_common host=#{S_HOST} uri=/dmr/audios/new 200 #{TOTAL}"
      data_count(q, s_date, e_date)
    end

    def view_count(s_date, e_date)
      stop_time = stop_time_view(s_date, e_date, '(mp4 OR smil)')
      q = "search sourcetype=wowza_access host=#{WOWZAHOST} play stream 200 dmr (mp4 OR smil)"
      data = stats(q, s_date, e_date)
      process_count(data, stop_time)
    end

    def licensed_video_view(s_date, e_date)
      q = "search sourcetype=rails host=#{RAILS_HOST} MediaController#view"
      filenames = process_filename(q, s_date, e_date)
      stop_time = stop_time_licensed_view(filenames, s_date, e_date)
      q = "search sourcetype=wowza_access host=#{WOWZAHOST} play stream 200 dmr (mp4 OR smil)"
      data = stats(q, s_date, e_date)
      process_count(data, stop_time)
    end

    def licensed_audio_view(s_date, e_date)
      q = "search sourcetype=rails host=#{RAILS_HOST} AudiosController#view"
      filenames = process_filename(q, s_date, e_date)
      stop_time = stop_time_licensed_view(filenames, s_date, e_date)
      q = "search sourcetype=wowza_access host=#{WOWZAHOST} play stream 200 dmr mp3"
      data = stats(q, s_date, e_date)
      process_audio_count(data, stop_time)
    end

    def audio_view_count(s_date, e_date)
      stop_time = stop_time_view(s_date, e_date, 'mp3')
      q = "search sourcetype=wowza_access host=#{WOWZAHOST} play stream 200 dmr mp3"
      data = stats(q, s_date, e_date)
      process_audio_count(data, stop_time)
    end

    def process_count(data, stop_time)
      tmp_view = {}
      data.each do |result|
        tmp = result['_raw'].to_s.split
        if stop_time.key?(process_id(tmp))
          time_diff = Time.diff(tmp[1], stop_time[process_id(tmp)])[:minute]
          tmp_view[process_id(tmp)] = time_diff if time_diff >= 5
        end
      end
      unique_count(tmp_view)
    end

    def process_audio_count(data, stop_time)
      tmp_view = {}
      data.each do |result|
        tmp = result['_raw'].to_s.split
        next unless stop_time.key?(process_id(tmp))
        time_diff = Time.diff(tmp[1], stop_time[process_id(tmp)])[:second]
        tmp_view[process_id(tmp)] = time_diff if time_diff >= 15
      end
      unique_count(tmp_view)
    end

    def unique_count(view)
      key_prefix = ''
      key_array = []
      view.keys.sort.each { |key|
        key_prefix = "#{key.split('-')[0]}-#{key.split('-')[2]}"
        key_array << key_prefix if !key_array.include?(key_prefix)
      }
      key_array.length
    end

    # rubocop:disable Metrics/CyclomaticComplexity
    # rubocop:disable Metrics/AbcSize
    def stop_time_view(s_date, e_date, type)
      q = "search sourcetype=wowza_access host=#{WOWZAHOST} stop stream 200 dmr #{type}"
      data = stats(q, s_date, e_date)
      files = {}
      tmp_course_id = nil
      data.each do |result|
        tmp_result = result['_raw'].to_s.split
        results = Media.where(file_name: tmp_result[7].gsub('MP4_Archive/', '')) if type != 'mp3'
        results = Audio.where(file_name: tmp_result[7].gsub('MP3_Archive/', '')) if type == 'mp3'
        if results.present? && !results.empty?
          report = Report.where(media_id: results.first.id.to_i).order('created_at DESC') if type != 'mp3'
          report = Audioreport.where(audio_id: results.first.id.to_i).order('created_at DESC') if type == 'mp3'
          tmp_course_id = report.first.course_id if !report.nil? && !report.first.nil?
        end
        next unless !tmp_course_id.nil?
        files[process_id(tmp_result)] = tmp_result[1]
      end
      files
    end
    # rubocop:enable Metrics/AbcSize
    # rubocop:enable Metrics/CyclomaticComplexity

    def process_id(row)
      "#{row[7]}-#{row[row.length - 18]}-#{row[16]}"
    end

    def convert_time(date, time)
      if DateTime.current.strftime('%m/%d/%Y').include?(date) && time.include?('PM')
        d_time = 'now'
      else
        d_time = DateTime.strptime("#{date} #{time}", '%m/%d/%Y %H:%M:%S%z')
      end
      d_time
    end
  end
end
