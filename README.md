# Digital Media Reserves

[![GitLab Pipeline Status](https://gitlab.com/ucsdlibrary/development/dmr/badges/trunk/pipeline.svg)](https://gitlab.com/ucsdlibrary/development/dmr/-/commits/trunk)

DMR is a Digital Media Reserves Tool to manage videos created for the UCSD campus.

## Requirements

* Ruby 2.2.3+
* git

## Installation

```
$ git clone git@github.com:ucsdlib/dmr.git
```

## Usage

1.Open project.

```
$ cd dmr
```

2.Checkout develop branch.

```
$ git checkout develop

```

3.Update DB.

```
$ bundle exec rake db:migrate
```

4.Update Test DB

```
$ bundle exec rake db:migrate RAILS_ENV=test
```

## Running DMR

* WEBrick

```
$ rails s
```

## DMR will be available at

```
http://localhost:3000/dmr/
```

## Running the Test Suites

```
$ bundle exec rake
```
## Deploying with GitLab ChatOps
This application is setup currently for deployments to be done via GitLab's
chatops feature.

The supported environments to deploy to, via Capistrano, are:
- staging
- production

Example: `/gitlab ucsdlibrary/development/dmr run production_deploy`

By default, Capistrano will deploy the specified default branch, which is
`trunk`. If you would like to specify a different branch or tag, you can pass
that as an argument to the slack command.

Example: `/gitlab ucsdlibrary/development/dmr run production_deploy 1.5.0`

The above command will deploy the `1.5.0` tag to `production`.

### Scheduling A Production Deployment
The application also supports setting up a GitLab [Scheduled
Pipeline][gitlab-schedule] to support production deployments during the
Operations service window (6-8am PST).

An example schedule using [Cron syntax][cron] might be `30 06 * * 2` which would deploy
the application at 06:30AM on Monday.

Make sure you set the `Cron timezone` in the schedule for `Pacific Time (US and
Canada)`

After the deploy, make sure you de-active or delete the Schedule.
Otherwise the application may accidentally deploy again the following week.

## Rollbacks with GitLab ChatOps
If a deployment gets far enough that it cannot be automatically rolled back, you
can make calls to rollback to the previous release. Internally, this uses the
Capistrano `deploy:rollback` task.

Example: `/gitlab ucsdlibrary/development/dmr run production_rollback`

[cron]:https://en.wikipedia.org/wiki/Cron
[gitlab-schedule]:https://docs.gitlab.com/ee/ci/pipelines/schedules.html
