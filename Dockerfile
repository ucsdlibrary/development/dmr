# renovate: datasource=docker depName=ruby versioning=ruby
ARG RUBY_VERSION=2.3.7
FROM ruby:$RUBY_VERSION-alpine as development

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  build-base \
  less \
  postgresql-dev \
  nodejs \
  npm \
  tzdata \
  vim \
  yarn \
  sqlite-dev \
  linux-headers \
  freetds-dev \
  libxml2-dev \
  libxslt \
  libxslt-dev \
  && rm -rf /var/cache/apk/*

WORKDIR /app

ENV EDITOR=vim
ENV RAILS_ENV=development
ENV RACK_ENV=development
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_ROOT=/app
ENV LANG=C.UTF-8

# Install gems
COPY Gemfile* /app/
RUN bundle config build.nokogiri --use-system-libraries && \
    gem update bundler && \
    bundle install --jobs "$(nproc)" --retry 2

COPY . /app

CMD ["rails", "s", "-b", "0.0.0.0"]

FROM ruby:$RUBY_VERSION-alpine as production

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  postgresql-dev \
  nodejs \
  npm \
  tzdata \
  vim \
  yarn \
  sqlite-dev \
  linux-headers \
  freetds-dev \
  libxml2-dev \
  libxslt \
  libxslt-dev \
  && rm -rf /var/cache/apk/*

WORKDIR /app

ENV APP_HOST=localhost:3000
ENV LANG=C.UTF-8
ENV RACK_ENV=production
ENV RAILS_ENV=production
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_ROOT=/app
ENV RAILS_SERVE_STATIC_FILES=true
ENV SECRET_KEY_BASE=something

COPY --from=development /app ./
COPY --from=development /usr/local/bundle /usr/local/bundle

RUN bundle config set without 'development test' \
    && bundle clean --force \
    # Remove unneeded files (cached *.gem, *.o, *.c)
    && find /usr/local/bundle -name "*.gem" -delete \
    && find /usr/local/bundle -name "*.c" -delete \
    && find /usr/local/bundle -name "*.o" -delete

RUN bundle exec rake assets:precompile

RUN rm -rf node_modules tmp/* log/* vendor/dictionary*

EXPOSE 3000

CMD ["rails", "s", "-b", "0.0.0.0"]
