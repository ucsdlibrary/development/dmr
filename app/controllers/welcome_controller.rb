#
# @author Vivian <tchu@ucsd.edu>
#
class WelcomeController < ApplicationController
  before_action :authorize, only: [:index]
  layout 'application'
  ##
  # Handles GET index request
  #
  # @return [String] the resulting webpage
  def index
    @menu_display = 'false'
    @search_box_display = 'false'
    @menu_display = nil if logged_in?
  end

  # GET /logout
  def logout
    render layout: false
  end
end
