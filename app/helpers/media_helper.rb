#
# @author Vivian <tchu@ucsd.edu>
#
require 'base64'
require 'pathname'

APP_ROOT = Pathname.new File.expand_path('../../../', __FILE__)
#
# This module supports the function in Media controller
#
module MediaHelper
  def grab_secure_token_url(filename, base_url)
    return unless filename
    token_end_time = wowza_token_end_time
    token_hash = wowza_token_hash(token_end_time, filename, base_url)
    "#{Rails.configuration.secure_token_name}endtime=#{token_end_time}&#{Rails.configuration.secure_token_name}hash=#{token_hash}".html_safe
  end

  def wowza_token_hash(end_time, filename, base_url)
    token_params = []
    token_params << Rails.configuration.secure_token_secret
    token_params << "#{Rails.configuration.secure_token_name}endtime=#{end_time}"
    token_params = token_params.sort
    stream = base_url.sub(%r{.*?\/}, '')
    hash_in = "#{stream}#{filename}?#{token_params.join('&')}"
    hash_out = Digest::SHA2.new(256).digest(hash_in.to_s)
    base64_encode(hash_out).gsub('+/', '-_').to_s
  end

  def wowza_token_end_time
    Time.zone = 'America/Los_Angeles'
    Time.now.to_i + 48.hours
  end

  def get_file_url(file_name, file_ext)
    temp_file = file_name.gsub('.mp4', file_ext)
    return nil unless File.exist?("#{Rails.configuration.smil_filepath}#{temp_file}")
    if file_ext.include?('smil')
      "#{Rails.configuration.wowza_baseurl_smil}#{temp_file}"
    elsif file_ext.include?('srt')
      "#{Rails.configuration.wowza_baseurl_smil.gsub('smil:', '')}#{temp_file}"
    end
  end

  def get_wowza_url(file_name, file_ext)
    get_file_url(file_name, file_ext) || "#{Rails.configuration.wowza_baseurl}#{file_name}"
  end
end
