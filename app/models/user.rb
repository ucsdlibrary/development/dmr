#
# @author Vivian <tchu@ucsd.edu>
# @author hweng@ucsd.edu
#
# Supports user login function
#
class User < ActiveRecord::Base
  def self.find_or_create_for_developer(_access_token)
    User.find_by(uid: 1, provider: 'developer') || User.create(uid: 1, provider: 'developer',
                                                               email: 'developer@ucsd.edu',
                                                               name: 'developer')
  end

  def self.find_or_create_for_shibboleth(access_token)
    begin
      uid = access_token.uid
      email = access_token['info']['email'] || "#{uid}@ucsd.edu"
      provider = access_token.provider
      name = access_token['info']['name']
      temp_user = User.find_by_email(email.strip)
      if temp_user && temp_user.uid != uid
        Rails.logger.info("Delete user: email #{email} - UID: #{temp_user.uid} - #{uid}")
        temp_user.delete
      end
    rescue StandardError => e
      logger.warn "shibboleth: #{e}"
    end

    User.find_by(uid: uid, provider: provider) || User.create(uid: uid, provider: provider,
                                                              email: email, name: name) unless uid.nil?
  end

  # TODO: replace this w/ lib-adbridge query
  # @param uid - sAMAccountName of the user
  def self.in_group?(uid)
    ldap_group = Rails.application.secrets.ldap_group
    adbridge_uri = Rails.application.secrets.lib_adbridge_uri
    route = "GroupMembers/#{ldap_group}/#{uid}"

    uri = URI("#{adbridge_uri}#{route}")

    req = Net::HTTP::Get.new(uri)
    req.basic_auth Rails.application.secrets.lib_adbridge_username,
                   Rails.application.secrets.lib_adbridge_password

    res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == 'https') {|http|
      http.request(req)
    }
    Rails.logger.debug("adbridge response for user #{uid} : #{res.body}")

    res.body == "true"
  rescue StandardError => e
    Rails.logger.error("User.in_group? with uid #{uid} error #{e} ")
    false
  end

  def developer?
    provider.eql?('developer')
  end

  def self.student?(id)
    UserDb.table_name = 'CourseUsers'
    q = 'lower(Username) = ?'
    result = UserDb.where(q, id.to_s.downcase)
    course_student = !result.empty? ? true : false
    Rails.logger.info("user #{id} is student? - #{course_student}")
    course_student
  end
end
